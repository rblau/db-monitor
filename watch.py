#!/usr/bin/env python3
import sys
import subprocess
from http.server import BaseHTTPRequestHandler, HTTPServer
import getpass
import argparse
import os

currentSchema = ''
graph = None
mysqldumpOptions = []
port = None

def main():
    global currentSchema, port, mysqldumpOptions, graph
    args = getArguments()

    port = args.port

    mysqldumpOptions = [
        'mysqldump',
        '--skip-quote-names',
        '--no-data',
        '--compact',
        args.database
    ]

    if args.user != None:
        mysqldumpOptions.append('-u' + args.user)

    if args.password:
        mysqldumpOptions.append('-p' + getpass.getpass())

    print('Running. Open http://localhost:' + str(args.port) + ' in your browser.')
    server_address = ('127.0.0.1', args.port)

    httpd = HTTPServer(server_address, RequestHandler)
    httpd.serve_forever()

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        global currentSchema, port, mysqldumpOptions, graph

        if self.path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()

            html = open(os.path.dirname(os.path.realpath(__file__)) + '/index.html', 'r').read()
            self.wfile.write(bytes(html, 'utf8'))
            return

        elif self.path.startswith('/graph.png'):
            self.send_response(200)
            self.send_header('Content-type', 'image/png')
            self.end_headers()

            p = subprocess.Popen(mysqldumpOptions, stdout=subprocess.PIPE)
            out, err = p.communicate()
            if out != currentSchema:
                graph = createImage(mysqldumpOptions, port)
                currentSchema = out

            self.wfile.write(graph)
            return

        else:
            self.send_response(404)
            self.send_header('Content-type','text/html')
            self.end_headers()
            return

    # Suppress HTTP log messages
    def log_message(self, format, *args):
        return

def getArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('database', help='database to use')
    parser.add_argument('-u', '--user', help='database user')
    parser.add_argument('-p', '--password', help='Prompt for a database password', action='store_true')
    parser.add_argument('-P', '--port', type=int, help='Port to serve to', default=7777)
    return parser.parse_args()

def createImage(mysqldumpOptions, port):
    sqlFilePath = '/tmp/db-monitor_dump_' + str(port) + '.sql'

    sqlFile  = open(sqlFilePath, 'w')
    subprocess.call(mysqldumpOptions, stdout=sqlFile)
    p = subprocess.Popen([
        'sqlt-graph',
        '-f', 'MySQL',
        '--color',
        '--fontsize=14',
        '--layout=neato',
        '--output-type', 'png',
        '--show-constraints',
        '--show-datatypes',
        '--show-sizes',
        sqlFilePath
    ], stdout=subprocess.PIPE)
    out, err = p.communicate()
    return out

if __name__ == '__main__':
    main()
