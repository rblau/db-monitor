# What is this?

A simple Python script providing a live visualization of a MySQL database.
Make changes to your database (in whatever way you prefer) and it will show you a graph of it inside your browser.


# Running

1. Run with python3: `./watch.py database`
2. Open http://localhost:7777 in your browser. It refreshes automatically.


# Prerequisites

Make sure you have all of the following installed:

* mysql
* python3
* sqlfairy (sqlt-graph)
  http://search.cpan.org/dist/SQL-Translator/script/sqlt-graph
